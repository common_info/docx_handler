import copy
from pathlib import Path
from typing import Optional
from zipfile import ZipFile, ZIP_DEFLATED

import docx
import lxml.etree
from docx.document import Document
from docx.enum.style import WD_STYLE_TYPE
# noinspection PyProtectedMember
from docx.styles.style import _TableStyle
from docx.styles.styles import Styles
from docx.table import Table
from loguru import logger
from lxml.etree import ElementBase
from lxml.etree import Element


class DocxDocument:
    def __init__(self, file: str):
        self._file: Path = Path(file)

    @property
    def file(self):
        return self._file

    @file.setter
    def file(self, value):
        self._file = value

    @property
    def document(self) -> Document:
        return docx.Document(self.file)

    @property
    def styles(self) -> Styles:
        return self.document.styles

    def add_styles(self):
        styles_to_add: list[_TableStyle] = list(filter(lambda x: x.type == WD_STYLE_TYPE.TABLE, self.styles))
        for style in styles_to_add:
            _style: _TableStyle = copy.deepcopy(style)
            if _style.name not in self.styles:
                self.styles.add_style(_style.name, _style.type, _style.builtin)
        return

    def save(self):
        self.document.save(self.file)


class DocxZipFileReader(DocxDocument):
    @property
    def zip_file(self):
        return ZipFile(self.file, "r", ZIP_DEFLATED, True)

    @property
    def filenames(self):
        return self.zip_file.namelist()

    def __getitem__(self, item):
        if item in self.filenames:
            with self.zip_file as zf:
                content = zf.read(item)
            return content
        else:
            logger.error("KeyError, getitem")
            return NotImplemented

    def get_content(self, item):
        return self[item].decode("utf8")

    def close(self):
        self.zip_file.close()


class DocxZipFileWriter(DocxDocument):
    @property
    def zip_file(self):
        return ZipFile(self.file, "w", ZIP_DEFLATED, True)

    def write(self, file: str, text):
        if isinstance(text, str):
            _text: bytes = text.encode()
        elif isinstance(text, bytes):
            _text: bytes = text
        else:
            return NotImplemented
        with self.zip_file as zf:
            with zf.open(file, "w") as f:
                f.write(_text)
        return

    def close(self):
        self.zip_file.close()


class DocxZipFile(DocxDocument):
    @classmethod
    def from_docx_document(cls, docx_document: DocxDocument):
        return cls(str(docx_document.file))

    @property
    def reader(self):
        return DocxZipFileReader(str(self.file))

    @property
    def writer(self):
        return DocxZipFileWriter(str(self.file))

    def close(self):
        self.reader.close()
        self.writer.close()


class DocPropertyManager(DocxZipFile):
    _archive_file = None
    _final_content: Optional[bytes] = None

    @classmethod
    def from_docx_zip_file(cls, docx_zip_file: DocxZipFile):
        return cls(str(docx_zip_file.file))

    @property
    def archive_file(self):
        return self._archive_file

    @archive_file.setter
    def archive_file(self, value):
        self._archive_file = value

    def file_content(self) -> bytes :
        return self.reader[self.archive_file]

    def human_file_content(self) -> str:
        return self.reader.get_content(self.archive_file)

    @property
    def xml_content(self) -> ElementBase:
        return lxml.etree.fromstring(self.file_content())

    def find_child(self, key: str, value: str) -> ElementBase:
        return list(filter(lambda x: x.get(key) == value, self.xml_content.getchildren()))[0]

    def get_final_el(self, key: str, value: str) -> ElementBase:
        return self.find_child(key, value).getchildren()[0]

    def get_text(self, key: str, value: str) -> str:
        return self.get_final_el(key, value).text

    def set_text(self, key: str, value: str, text: str):
        self.get_final_el(key, value).text = text
        return

    @property
    def final_content(self):
        return self._final_content

    @final_content.setter
    def final_content(self, value):
        self._final_content = value

    #TODO
    def save(self):
        super().writer.write(self.archive_file, self.final_content)
        super().close()


class TableManager(DocxDocument):
    def __init__(self, file: str):
        super().__init__(file)
        self._new_file: Path = self.file.with_name(f"{self.file.stem}_new.docx")
        self._tables: Optional[tuple[Table, ...]] = None

    @classmethod
    def from_docx_document(cls, docx_document: DocxDocument):
        return cls(str(docx_document.file))

    @property
    def new_file(self):
        return self._new_file

    @new_file.setter
    def new_file(self, value):
        self._new_file = value

    @property
    def tables(self):
        return self._tables

    @tables.setter
    def tables(self, value):
        self._tables = value

    def get_tables(self):
        _tables: list[Table] = self.document.tables
        self.tables = _tables
        return self.document.tables

    def __iter__(self):
        return iter(self.tables)

    def __contains__(self, item):
        return item in self.tables

    def __getitem__(self, item):
        return self.tables.__getitem__(item)


def main():
    path_pmi: str = "file_PMI_document"
    pmi_document: DocxDocument = DocxDocument(path_pmi)
    table_manager: TableManager = TableManager.from_docx_document(pmi_document)
    _tables: list[Table] = table_manager.get_tables()

    # get DocProperty
    path_template: str = "file_template"
    temp_file: DocxDocument = DocxDocument(path_template)
    docx_zip_file: DocxZipFile = DocxZipFile.from_docx_document(temp_file)
    doc_property_manager: DocPropertyManager = DocPropertyManager.from_docx_document(docx_zip_file)
    # root: ElementBase = doc_property_manager.xml_content()
    element: ElementBase = doc_property_manager.get_child_by_filter("property", "name", "ProjectName")[0]
    child: ElementBase
    for child in element.getchildren():
        child.text = "new_doc_property_value"


if __name__ == "__main__":
    doc_property_name: str = "DocFullName"
    doc_path: str = "C:\\Users\\tarasov-a\\Desktop\\mCore.SSW5.Web.AdminGuide_2022_v.2.docx"
    document: DocxDocument = DocxDocument(doc_path)
    zip_file: DocxZipFile = DocxZipFile.from_docx_document(document)
    property_manager: DocPropertyManager = DocPropertyManager.from_docx_document(zip_file)
    property_manager.archive_file = "docProps/custom.xml"

    root = property_manager.xml_content
    print(property_manager.get_final_el("name", doc_property_name))
    print(property_manager.get_text("name", doc_property_name))

    new_el: ElementBase = Element("{http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes}lpwstr")
    new_el.text = "Check property"
    print(new_el.text)
    child: ElementBase = property_manager.find_child("name", doc_property_name)
    print(f"child = {child}")
    print(property_manager.get_final_el("name", doc_property_name).text)
    child.remove(child[0])
    child.append(new_el)
    print(child[0].text)

    property_manager.final_content = lxml.etree.tostring(property_manager.xml_content, encoding="utf-8", xml_declaration=True)
    print(property_manager.final_content.decode("utf-8"))
    zip_file.writer.write(property_manager.archive_file, property_manager.final_content)
    zip_file.close()
    # table_manager: TableManager = TableManager.from_docx_document(document)
    # table_manager.get_tables()
    # print(table_manager.tables[0])

