from pathlib import Path
from typing import Union
from docx import Document as Doc
from docx.document import Document
# noinspection PyProtectedMember
from docx.styles.style import _TableStyle
from docx.table import Table

from custom_document import CustomDocument


class TableManager(CustomDocument):
    def __init__(self, path: Union[str, Path], *, names):
        super().__init__(path, **names)
        self._names = None

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, value):
        self._names = value

    @property
    def document(self) -> Document:
        return Doc(str(self.path))

    @property
    def tables(self) -> list[Table]:
        return self.document.tables

    def __getitem__(self, item: int):
        return self.tables.__getitem__(item)

    def __len__(self):
        return len(self.tables)

    def dict_tables(self) -> dict[str, Table]:
        return dict(zip(self.names, self.tables))

    def _prepared_table(self, item: int) -> tuple[int, int, _TableStyle]:
        table: Table = self.__getitem__(item)
        # noinspection PyTypeChecker
        return len(table.rows), len(table.columns), table.style

    def add_table(self, custom_document: CustomDocument, table_index: int):
        custom_document.document.add_table(*self._prepared_table(table_index))
