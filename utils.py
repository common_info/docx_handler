from pathlib import Path

import docx
from docx.document import Document

names = (
    "2) Перехват",
    "3) CW",
    "7) Cетевая конференция",
    "8) Приоритетный абонент",
    "9) Горячая линия",
    "10) Transfer",
    "11) Группы поиска",
    "12) Журнал вызовов",
    "13) DND",
    "14) CLIR",
    "15) Подмена номера",
    "16) Подмена идентификатора",
    "17) Перехват вызова с помощью кода",
    "18) Отключение микрофона",
    "19) Обратный вызов по занятости",
    "20) Обратный вызов по неответу",
    "21) Набор последнего номера",
    "22) Черный_Белый список",
    "23) Отображение имени абонента",
    "24) Отключение анонимного вызова",
    "25) Отключение вызова без ответа",
    "26) Вмешательство",
    "27) Запрет вмешательства",
    "28) Hold",
    "29) Быстрый набор",
    "30) BLF",
    "31) CUG",
    "32) Автодозвон на занятого внешнего абонента",
    "33) IVR",
    "34) DTMF",
    "35) Спец сигнал",
    "36) Различные сигналы вызова",
    "38) Интерком",
    "39) Парковка",
    "40) SharedLine",
    "41) Chatroom",
    "42) Автоответ",
    "43) МОН",
    "44) DN Translit")


"CLIR ": (13, "14) CLIR.docx"),
"MaskingCgPN": (14, "15) Подмена номера.docx"),
"DisplayName": (22, "16) Подмена идентификатора.docx"),
"CallIDInterception ": (15, "17) Перехват вызова с помощью кода.docx"),
"Mute ": (16, "18) Отключение микрофона.docx"),
"CallBackUserBusy ": (17, "19) Обратный вызов по занятости.docx"),
"CallBackNoReply": (18, "20) Обратный вызов по неответу.docx"),
"CallBack ": (19, "21) Набор последнего номера.docx"),
"BlackWhiteLists": (20, "22) Черный_Белый список.docx"),
"IdentifierDisplayName": (21, "23) Отображение имени абонента.docx"),
"RejectAnonymous": (23, "24) Отключение анонимного вызова.docx"),
"RejectNoReply": (24, "25) Отключение вызова без ответа.docx"),
"Interception ": (25, "26) Вмешательство.docx"),
"RestrictInterception ": (26, "27) Запрет вмешательства.docx"),
"Hold ": (27, "28) Hold.docx"),
"FastCall ": (28, "29) Быстрый набор.docx"),
"BLF": (29, "30) BLF.docx"),
"CUG": (30, "31) CUG.docx"),
"IVR": (31, "33) IVR.docx"),
"SpecSignal ": (32, "35) Спец сигнал.docx"),
"DifferentSpecSignals ": (33, "36) Различные сигналы вызова.docx"),
"Intercom ": (34, "38) Интерком.docx"),
"CallParking": (35, "39) Парковка.docx"),
"SharedLine ": (36, "40) SharedLine.docx"),
"Chatroom ": (37, "41) Chatroom.docx"),
"AutoResponse ": (38, "42) Автоответ.docx"),
"MusicOnHold": (39, "43) МОН.docx"),
"DisplayNameLiteration": (40, "44) DN Translit.docx")


def get_names():
    path = "C:\\Users\\tarasov-a\Desktop\\Переработанные инструкции\\"
    for name in names:
        file = list(Path(path).joinpath(name).iterdir())[0]
        file.rename(file.with_name(f"{name}.docx"))
        print(file.name)


def get_tables():
    path: str = "C:\\Users\\tarasov-a\\Desktop\\27.12.2022\\ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx"
    document: Document = docx.Document(path)
    for index, table in enumerate(document.tables):
        print(f"{index}", table.cell(1, 0).text)



if __name__ == "__main__":
    get_names()
