from pathlib import Path
from typing import Union

from docx.text.paragraph import Paragraph

from custom_document import CustomDocument


class DocxFile(CustomDocument):
    _text_start: str = "Для активации"
    _text_end: str = "Проверка ДВО."

    @property
    def _document_plist(self) -> list[Paragraph]:
        return self.document.paragraphs

    @property
    def _p_index_start(self) -> int:
        for index, p in enumerate(self._document_plist):
            if p.text.startswith(self._text_start):
                return index

    def p_replace(self):
        for index, p in enumerate(self._document_plist):
            if p.text == "insert_":
                return index

    @property
    def _p_index_end(self) -> int:
        for index, p in enumerate(self._document_plist):
            if p.text.endswith(self._text_end):
                return index + 1

    def __iter__(self):
        return iter(self.plist())

    def plist(self) -> list[Paragraph]:
        return self._document_plist[self._p_index_start:self._p_index_end]

    def __reversed__(self):
        return reversed(self.plist())

    def __len__(self):
        return len(self.plist())

    def add_plist(self, custom_document: CustomDocument):
        index: int = self.p_replace()
        p_empty: Paragraph = custom_document.document.paragraphs[index].insert_paragraph_before()
        for p in reversed(self):
            new_p: Paragraph = p_empty.insert_paragraph_before(p.text, p.style)
            new_p.insert_paragraph_before()
        return


class TextManager:
    def __init__(self, path: Union[str, Path]):
        if isinstance(path, str):
            path = Path(path)
        self._path: Path = path.resolve()

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    def __len__(self):
        return len(self.subfolders)

    def __iter__(self):
        return self.path.iterdir()

    @property
    def subfolders(self) -> tuple[Path, ...]:
        return tuple(iter(self))

    def __getitem__(self, item: int):
        return self.subfolders.__getitem__(item)

    def _file(self, item: int) -> Path:
        return list(self.__getitem__(item).iterdir())[0]

    def docx_files(self) -> dict[str, DocxFile]:
        return {self._file(item).stem: DocxFile(self._file(item)) for item in range(len(self))}

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.path})>"

    def __str__(self):
        return f"{self.__class__.__name__} -- {self.path}"

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)