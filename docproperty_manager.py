from pathlib import Path
from typing import Union, Literal, Optional
from zipfile import ZipFile, is_zipfile, BadZipFile, ZIP_DEFLATED

from loguru import logger
from lxml import etree
from lxml.etree import ElementBase

from custom_document import CustomDocument


class XMLManager:
    def __init__(self, content: bytes):
        self._content: bytes = content

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.content == other.content
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.content != other.content
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.content)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    def root(self) -> ElementBase:
        return etree.fromstring(self.content)

    @property
    def properties(self) -> list[ElementBase]:
        return self.root().getchildren()

    def find_property(self, name: str) -> ElementBase:
        for item in self.root().getchildren():
            if item.get("name", None) == name:
                print(f"item = {item.tag}")
                return item

    def set_text(self, name: str, text: str):
        el: ElementBase = self.find_property(name)
        child: ElementBase = el.getchildren().__getitem__(0)
        print(f"child = {child.text}")
        child.text = text
        print(f"child = {child.text}")
        self.
        print(f'root = '
              f''
              f''
              f''
              f''
              f'{etree.tostring(self.root(), encoding="utf8", xml_declaration=True, standalone=True).decode("utf-8")}')

    def to_bytes(self):
        self.content = etree.tostring(self.root(), encoding="utf8", xml_declaration=True, standalone=True)


class ZipFileManager(CustomDocument):
    file_name: str = "docProps/custom.xml"

    def __init__(self, path: Union[str, Path]):
        super().__init__(path)
        self._mode: Literal["w", "r", "a", "x"] = "r"
        self._zip_file: Optional[ZipFile] = ZipFile(self.path, self.mode, ZIP_DEFLATED)
        self._content: bytes = b""

        if not is_zipfile(self.path):
            logger.error("Invalid zip file")
            raise BadZipFile

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self._mode = value

    @property
    def zip_file(self):
        return self._zip_file

    @zip_file.setter
    def zip_file(self, value):
        self._zip_file = value

    def zip_file_reader(self):
        self.zip_file.close()
        self.mode = "r"
        self.zip_file = ZipFile(self.path, self.mode, ZIP_DEFLATED)
        return

    def zip_file_writer(self):
        self.zip_file.close()
        self.mode = "w"
        self.zip_file = ZipFile(self.path, self.mode, ZIP_DEFLATED)
        return

    def filenames(self):
        return self.zip_file.namelist()

    def get_file_content(self):
        self.zip_file_reader()
        with self.zip_file.open(self.file_name, self.mode) as zf:
            content: bytes = zf.read()
        self.content = content

    def set_file_content(self):
        self.zip_file_writer()
        with self.zip_file.open(self.file_name, self.mode) as zf:
            # noinspection PyTypeChecker
            zf.write(self.content)
        return

    def close(self):
        self.zip_file.close()

    def get_content(self, file: str):
        self.zip_file_reader()
        self.zip_file = ZipFile(self.path, "r", ZIP_DEFLATED)
        with self.zip_file as zf:
            content: bytes = zf.read(file)
        self.content = content

    def unpack(self) -> Path:
        self.zip_file_reader()
        pack: Path = self.path.parent.joinpath("new_folder")
        self.zip_file.extractall(pack)
        return pack

    @classmethod
    def make_zip_file(cls, directory: Union[str, Path], name: str):
        if isinstance(directory, str):
            directory: Path = Path(directory)
        with ZipFile(name, "w", ZIP_DEFLATED) as zf:
            for folder, subfolders, filenames in os.walk(directory):
                for filename in filenames:
                    file = os.path.join(folder, filename)
                    arch_file = os.path.relpath(file, directory)
                    zf.write(file, arch_file)
        return Path(name).resolve()
