from pathlib import Path
from typing import Union
from docx import Document as Doc
from docx.document import Document


class CustomDocument:
    def __init__(self, path: Union[str, Path], **kwargs):
        if isinstance(path, str):
            path = Path(path)
        self._path: Path = path.resolve()
        for k, v in kwargs:
            setattr(self, k, v)
        self._kwargs = kwargs

    def __repr__(self):
        _kwargs = ", ".join([f"{k}={v}" for k, v in self._kwargs.items()])
        return f"<{self.__class__.__name__}({self.path}, {_kwargs})>"

    def __str__(self):
        _kwargs = ", ".join([f"{k}: {v}" for k, v in self._kwargs.items()])
        return f"{self.__class__.__name__} -- {self.path}\n{_kwargs}"

    def __len__(self):
        return len(self._kwargs)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def document(self) -> Document:
        return Doc(str(self.path))
