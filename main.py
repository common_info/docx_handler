import glob
import os
import pathlib
import sys
import zipfile
from enum import Enum
from os.path import basename
from pathlib import Path
from typing import Union, Optional, NamedTuple, Iterable
from zipfile import ZipFile, ZIP_DEFLATED, BadZipFile

import lxml.etree
from docx import Document as Doc
from docx.document import Document
from docx.table import Table
from docx.text.paragraph import Paragraph
from loguru import logger
from lxml.etree import ElementBase, ElementTree


class TestingVAS(NamedTuple):
    testing_table: int
    vas_file: str

    def __repr__(self):
        return f"<{self.__class__.__name__}({self.testing_table}, {self.vas_file})>"

    def __str__(self):
        return f"testing table {self.testing_table} | vas file {self.vas_file}"


class FileMapping(dict):
    _file_mapping: dict[str, tuple[int, str]] = {
        "CLIR ": (13, "14) CLIR"),
        "MaskingCgPN": (14, "15) Подмена номера"),
        "DisplayName": (22, "16) Подмена идентификатора"),
        "CallIDInterception ": (15, "17) Перехват вызова с помощью кода"),
        "Mute ": (16, "18) Отключение микрофона"),
        "CallBackUserBusy ": (17, "19) Обратный вызов по занятости"),
        "CallBackNoReply": (18, "20) Обратный вызов по неответу"),
        "CallBack ": (19, "21) Набор последнего номера"),
        "BlackWhiteLists": (20, "22) Черный_Белый список"),
        "IdentifierDisplayName": (21, "23) Отображение имени абонента"),
        "RejectAnonymous": (23, "24) Отключение анонимного вызова"),
        "RejectNoReply": (24, "25) Отключение вызова без ответа"),
        "Interception ": (25, "26) Вмешательство"),
        "RestrictInterception ": (26, "27) Запрет вмешательства"),
        "Hold ": (27, "28) Hold"),
        "FastCall ": (28, "29) Быстрый набор"),
        "BLF": (29, "30) BLF"),
        "CUG": (30, "31) CUG"),
        "IVR": (31, "33) IVR"),
        "SpecSignal ": (32, "35) Спец сигнал"),
        "DifferentSpecSignals ": (33, "36) Различные сигналы вызова"),
        "Intercom ": (34, "38) Интерком"),
        "CallParking": (35, "39) Парковка"),
        "SharedLine ": (36, "40) SharedLine"),
        "Chatroom ": (37, "41) Chatroom"),
        "AutoResponse ": (38, "42) Автоответ"),
        "MusicOnHold": (39, "43) МОН"),
        "DisplayNameLiteration": (40, "44) DN Translit")
    }

    def __call__(self, *args, **kwargs):
        return self.converted_mapping

    @property
    def converted_mapping(self) -> dict[str, TestingVAS]:
        _dict_file_mapping: dict[str, TestingVAS] = dict()
        for k, values in self._file_mapping.items():
            int_value, str_value = values
            testing_vas: TestingVAS = TestingVAS(int_value, str_value)
            _dict_file_mapping[k] = testing_vas
        return _dict_file_mapping

    def __getitem__(self, item):
        return self.converted_mapping.get(item)

    def __setitem__(self, key, value):
        self.converted_mapping.__setitem__(key, value)

    def get_table(self, item) -> int:
        return self.converted_mapping.get(item).testing_table

    def get_vas_file(self, item) -> str:
        return self.converted_mapping.get(item).vas_file


class FolderManager:
    def __init__(self, path: Union[str, Path]):
        if isinstance(path, str):
            path = Path(path)
        self._path: Path = path.resolve()

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    def __iter__(self):
        return self.path.iterdir()

    def content(self):
        return tuple(item for item in self.path.iterdir())

    def folders(self) -> dict[str, Path]:
        return {item.stem: item for item in self.content()}

    def __contains__(self, item):
        return item in self.folders().keys()

    def __getitem__(self, item):
        el = None
        for name in self.folders().keys():
            if name.startswith(f"{item}"):
                el = name
        return self.folders().get(el, None)

    def file(self, item):
        files = tuple(item for item in self.__getitem__(item).iterdir())
        return files[0]

    def document(self, item) -> Document:
        return Doc(str(self.file(item)))

    def paragraphs_item(self, item) -> list[Paragraph]:
        return self.document(item).paragraphs

    def find_start_paragraph_item(self, item, text: str) -> int:
        for index, p in enumerate(self.paragraphs_item(item)):
            if p.text.startswith(text):
                return index

    def find_end_paragraph_item(self, item, text: str) -> int:
        for index, p in enumerate(self.paragraphs_item(item)):
            if p.text.endswith(text):
                return index + 1

    def get_paragraphs_item(self, item, text_start: str, text_end: str) -> list[Paragraph]:
        from_index: int = self.find_start_paragraph_item(item, text_start)
        to_index: int = self.find_end_paragraph_item(item, text_end)
        return self.document(item).paragraphs[from_index:to_index]


    def get_paragraphs(self, text_start: str, text_end: str) -> dict[str, list[Paragraph]]:
        _dict_content: dict[str, list[Paragraph]] = dict()
        for k, v in self.folders().items():
            _dict_content[k] = []
            pars: list[Paragraph] = self.get_paragraphs_item(str(v), text_start, text_end)
            _dict_content[k].extend(pars)
        return _dict_content


class TableManager:
    def __init__(self, path: Union[str, Path], names: list[str] = None):
        if names is None:
            names = []
        if isinstance(path, str):
            path = Path(path)
        self._path: Path = path.resolve()
        self._names: list[str] = names

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def names(self):
        return self._names

    @names.setter
    def names(self, value):
        self._names = value

    @property
    def document(self) -> Document:
        return Doc(str(self.path))

    @property
    def tables(self) -> list[Table]:
        return self.document.tables

    def iter_tables(self):
        return iter(self.tables)

    def iter_dict_tables(self):
        return iter(self.dict_tables().values())

    def iter_dict_names(self):
        return iter(self.dict_tables().keys())

    def get_tables(self, item):
        return self.tables.__getitem__(item)

    def get_dict_tables(self, item):
        return self.dict_tables().get(item)

    def dict_tables(self):
        return dict(zip(self.names, self.tables))


class XMLManager:
    def __init__(self, content: bytes):
        self._content: bytes = content

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.content == other.content
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.content != other.content
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.content)

    @property
    def content(self):
        return self._content

    @content.setter
    def content(self, value):
        self._content = value

    def root(self) -> ElementBase:
        return lxml.etree.fromstring(self.content)

    @property
    def properties(self) -> list[ElementBase]:
        return self.root().getchildren()

    def find_property(self, name: str) -> ElementBase:
        for item in self.root().getchildren():
            if item.get("name", None) == name:
                print(f"item = {item.tag}")
                return item

    def set_text(self, name: str, text: str):
        el: ElementBase = self.find_property(name)
        child: ElementBase = el.getchildren().__getitem__(0)
        print(f"child = {child.text}")
        child.text = text
        print(f"child = {child.text}")
        self.
        print(f'root = '
              f''
              f''
              f''
              f''
              f'{lxml.etree.tostring(self.root(), encoding="utf8", xml_declaration=True, standalone=True).decode("utf-8")}')

    def to_bytes(self):
        self.content = lxml.etree.tostring(self.root(), encoding="utf8", xml_declaration=True, standalone=True)


class ZipFileMode(Enum):
    READER = "r"
    WRITER = "w"
    APPENDER = "a"
    NONE = None

    def __repr__(self):
        return f"<{self.__class__.__name__}.{self.value})>"

    def __str__(self):
        return f"{self.__class__.__name__}[{self.name}]"


class ZipFileManager:
    file_name: str = "docProps/custom.xml"

    def __init__(self, path: Union[str, Path]):
        if isinstance(path, str):
            path = Path(path)
        self._path: Path = path.resolve()
        self._mode: ZipFileMode = ZipFileMode.NONE
        self._zip_file: Optional[ZipFile] = None

        if not zipfile.is_zipfile(self.path):
            logger.error("Invalid zip file")
            raise BadZipFile

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.path)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        self._mode = value

    @property
    def zip_file(self):
        return self._zip_file

    @zip_file.setter
    def zip_file(self, value):
        self._zip_file = value

    def zip_file_reader(self):
        if self.mode != ZipFileMode.READER:
            if self.zip_file is not None:
                self.zip_file.close()
            self.mode = ZipFileMode.READER
            self.zip_file = ZipFile(self.path, self.mode.value, ZIP_DEFLATED)
        return

    def zip_file_writer(self):
        if self.mode != ZipFileMode.WRITER:
            if self.zip_file is not None:
                self.zip_file.close()
            self.mode = ZipFileMode.WRITER
            self.zip_file = ZipFile(self.path, self.mode.value, ZIP_DEFLATED)
        return

    def zip_file_appender(self):
        if self.mode != ZipFileMode.APPENDER:
            if self.zip_file is not None:
                self.zip_file.close()
            self.mode = ZipFileMode.APPENDER
            self.zip_file = ZipFile(self.path, "a", ZIP_DEFLATED)
        return

    def filenames(self):
        return self.zip_file.namelist()

    def get_file_content(self):
        self.zip_file_reader()
        with self.zip_file as zf:
            # noinspection PyTypeChecker
            with zf.open(self.file_name, self.mode.value) as f:
                content: bytes = f.read()
        return content

    def set_file_content(self, content: bytes):
        self.zip_file_writer()
        with self.zip_file as zf:
            # noinspection PyTypeChecker
            with zf.open(self.file_name, self.mode.value) as f:
                f.write(content)
        return

    def close(self):
        self.zip_file.close()

    def get_content(self, file: str):
        self.zip_file_reader()
        self.zip_file = ZipFile(self.path, "r", ZIP_DEFLATED)
        with self.zip_file as zf:
            content: bytes = zf.read(file)
        return content

    def unpack(self) -> Path:
        self.zip_file_reader()
        self.zip_file = ZipFile(self.path, "r", ZIP_DEFLATED)
        pack: Path = self.path.parent.joinpath("new_folder")
        self.zip_file.extractall(pack)
        return pack

    def add_file(self, file: str, data: bytes):
        self.zip_file_appender()
        with self.zip_file as zf:
            zf.writestr(file, data, ZIP_DEFLATED)
        return

    @classmethod
    def make_zip_file(cls, directory: Union[str, Path], name: str):
        if isinstance(directory, str):
            directory: Path = Path(directory)
        with ZipFile(name, "w", ZIP_DEFLATED) as zf:
            for folder, subfolders, filenames in os.walk(directory):
                for filename in filenames:
                    file = os.path.join(folder, filename)
                    arch_file = os.path.relpath(file, directory)
                    zf.write(file, arch_file)
        return Path(name).resolve()


class DocxDocument:
    def __init__(self, path: Union[str, Path]):
        if isinstance(path, str):
            path = Path(path).resolve()
        self._path = path

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def file_path(self):
        return str(self.path)

    def document(self) -> Document:
        return Doc(self.file_path)

    def add_table(self, table: Table):
        # noinspection PyTypeChecker
        t: Table = self.document().add_table(len(table.rows), len(table.columns), table.style)
        t.style = table.style
        return

    def add_paragraph(self, paragraphs: Iterable[Paragraph] = None):
        if paragraphs is None:
            return
        for paragraph in paragraphs:
            p: Paragraph = self.document().add_paragraph(paragraph.text, paragraph.style)
        return

    def save(self):
        self.document().save(self.file_path)
        return


def main():
    text_start: str = "Для активации"
    text_end: str = "Проверка ДВО."

    if sys.platform == "win32":
        template_file: str = "C:\\User\\tarasov-a\\Desktop\\Шаблон инструкции.docx"
        table_file: str = "C:\\Users\\tarasov-a\\Desktop\\ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx"
        folder: str = "C:\\Users\\tarasov-a\\Desktop\\Переработанные инструкции\\"
    else:
        template_file: str = "/Users/user/Desktop/Шаблон инструкции.docx"
        table_file: str = "/Users/user/Desktop/ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx"
        folder: str = "/Users/user/Desktop/Переработанные инструкции/"

    template_file_manager: ZipFileManager = ZipFileManager(template_file)
    template_file_manager.zip_file_reader()
    print(template_file_manager.filenames())
    content: bytes = template_file_manager.get_file_content()

    xml_manager: XMLManager = XMLManager(content)
    xml_manager.set_text("VasName", "Check Value")
    xml_manager.to_bytes()

    new_template_file_manager: ZipFileManager = template_file_manager.duplicate()
    new_template_file_manager.add_file(new_template_file_manager.file_name, xml_manager.content)

    table_manager: TableManager = TableManager(table_file)

    folder_manager: FolderManager = FolderManager(folder)

    for table_index, file_name in FileMapping().converted_mapping.values():
        table: Table = table_manager.get_tables(table_index)
        file_path = folder_manager.path.joinpath(file_name).joinpath(f"{file_name}.docx")
        text: str = folder_manager.get_paragraphs_item(file_name, text_start, text_end)

        new_path = file_path.with_stem(f"{file_name}_new")
        docx_document = DocxDocument(new_path)
        docx_document.add_paragraph(text)
        docx_document.add_table(table)
        docx_document.save()

    print(content.decode("utf8"))
    template_file_manager.close()


if __name__ == "__main__":
    text_start: str = "Для активации"
    text_end: str = "Проверка ДВО."

    # if sys.platform == "win32":
    #     template_file: str = "C:\\User\\tarasov-a\\Desktop\\Шаблон инструкции.docx"
    #     table_file: str = "C:\\Users\\tarasov-a\\Desktop\\ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx"
    #     folder: str = "C:\\Users\\tarasov-a\\Desktop\\Переработанные инструкции\\"
    # else:
    #     template_file: str = "/Users/user/Desktop/Шаблон инструкции.docx"
    #     table_file: str = "/Users/user/Desktop/ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx"
    #     folder: str = "/Users/user/Desktop/Переработанные инструкции/"

    zip_file_manager: ZipFileManager = ZipFileManager(
        "C:\\Users\\tarasov-a\\Desktop\\CallFlow.Techdescription.UserGuide_2022_v3_0_checked.docx")
    # print(zip_file_manager.get_file_content())
    zip_file_manager.zip_file = ZipFile(zip_file_manager.path, "r", ZIP_DEFLATED)
    content: bytes = zip_file_manager.get_file_content()
    # print(content.decode("utf-8"))
    new_path = zip_file_manager.unpack()
    # print(new_path)

    xml_manager: XMLManager = XMLManager(content)
    xml_manager.set_text("DocFullName", "VAS NAME")
    xml_manager.to_bytes()

    doc_props_file: Path = new_path.joinpath(Path(ZipFileManager.file_name))
    with open(doc_props_file, "wb") as f:
        f.write(xml_manager.content)

    ZipFileManager.make_zip_file(new_path, "check.docx")

    docx_document: DocxDocument = DocxDocument("check.docx")
    table_manager: TableManager = TableManager("C:\\Users\\tarasov-a\\Desktop\\ПМИ_Лахта_ПРОТЕЙ_релиз_1.0_red_1.docx")
    folder_manager: FolderManager = FolderManager("C:\\Users\\tarasov-a\\Desktop\\Переработанные инструкции\\")

    testing_vas: TestingVAS = FileMapping().converted_mapping.get("BLF")

    list_p: list[Paragraph] = folder_manager.get_paragraphs_item(testing_vas.vas_file, text_start, text_end)
    # print(f"list_p = {list_p}")

    docx_document.add_paragraph(list_p)
    docx_document.add_table(table_manager.get_tables(testing_vas.testing_table))
    docx_document.save()

